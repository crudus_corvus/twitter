# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
# from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

from .models import Author


class AuthorCreationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password_repeat = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Author
        fields = ('nickname', 'email', 'is_staff', 'is_superuser', 'follow',)

    def clean_password_repeat(self):
        password = self.cleaned_data.get('password')
        password_repeat = self.cleaned_data.get('password_repeat')

        if password and password_repeat and password != password_repeat:
            raise forms.ValidationError('Пароли не совпадают')

        return password_repeat

    def save(self, commit=True):
        user = super(AuthorCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])

        if commit:
            user.save()

        return user


class AuthorChangeForm(forms.ModelForm):
    new_password = forms.CharField(widget=forms.PasswordInput, required=False)
    new_password_repeat = forms.CharField(widget=forms.PasswordInput, required=False)

    class Meta:
        model = Author
        fields = '__all__'

    def clean_new_password_repeat(self):
        new_password = self.cleaned_data.get('new_password')
        new_password_repeat = self.cleaned_data.get('new_password_repeat')

        if new_password and new_password_repeat and new_password != new_password_repeat:
            raise forms.ValidationError('Пароли не совпадают')

        return new_password_repeat

    def save(self, commit=True):
        user = super(AuthorChangeForm, self).save(commit=False)

        if self.cleaned_data['new_password']:
            user.set_password(self.cleaned_data['new_password'])
            if commit:
                user.save()

        return user


@admin.register(Author)
class AuthorAdmin(UserAdmin):
    form = AuthorChangeForm
    add_form = AuthorCreationForm

    list_display = ('id', 'nickname', 'email', 'name', 'photo', 'date_joined', 'is_staff', 'is_superuser',)
    list_display_links = ('id', 'nickname', 'email',)
    list_filter = []

    fieldsets = (
        (None, {
            'fields': (('nickname', 'date_joined',), 'email', 'name', 'photo', 'follow',)
        }),
        ('Управление доступами', {
            'fields': ('is_superuser', 'is_staff', 'is_blocked',)
        }),
        ('Пароль', {
            'classes': ('wide',),
            'fields': ('new_password', 'new_password_repeat')
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nickname', 'email', ('is_staff', 'is_superuser',), 'password', 'password_repeat')}),
    )
    search_fields = ['nickname', 'email', 'name', ]
    readonly_fields = ('date_joined',)
    ordering = ('-id',)
