# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import registration.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, verbose_name='last login', null=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('nickname', models.SlugField(unique=True)),
                ('email', models.EmailField(unique=True, max_length=255, db_index=True)),
                ('name', models.CharField(max_length=50)),
                ('photo', models.ImageField(upload_to=registration.models.generate_photoname, default='/static/noimage.png', blank=True)),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='Дата регистрации')),
                ('is_staff', models.BooleanField(default=False, verbose_name='Имеет доступ к административной панели сайта')),
                ('is_blocked', models.BooleanField(default=False, help_text='Поставьте галочку, чтобы заблокировать пользователя', verbose_name='Заблокирован')),
                ('follow', models.ManyToManyField(blank=True, related_name='following_for', verbose_name='Мои подписки', to=settings.AUTH_USER_MODEL)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', blank=True, to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', help_text='Specific permissions for this user.', blank=True, to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
