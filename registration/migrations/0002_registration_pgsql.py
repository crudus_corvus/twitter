# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def load_fixture(apps, schema_editor):
    from django.core.management import call_command
    call_command('loaddata', 'registration.json')


def do_nothing(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
	migrations.RunPython(load_fixture, reverse_code=do_nothing),
    ]

