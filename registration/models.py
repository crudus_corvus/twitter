# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


class AuthorManager(BaseUserManager):

    def create_user(self, nickname, email, password):

        if not nickname:
            raise ValueError('Никнейм: обязательное поле')

        if not email:
            raise ValueError('E-mail: обязательное поле')

        if not password:
            raise ValueError('Пароль: обязательное поле')

        user = self.model(
            nickname=nickname,
            email=AuthorManager.normalize_email(email),
        )
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, nickname, email, password):
        user = self.create_user(
            nickname,
            email,
            password,
        )

        user.is_staff = True
        user.is_superuser = True
        user.save()

        return user


def generate_photoname(self, imgname):
    ext = imgname.split('.')[-1]
    url = "%s/photo.%s" % (self.nickname, ext)
    return url


class Author(AbstractBaseUser, PermissionsMixin):
    nickname = models.SlugField(unique=True, db_index=True)
    email = models.EmailField(max_length=255, db_index=True, unique=True)
    name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to=generate_photoname, blank=True, default='/static/noimage.png')
    date_joined = models.DateTimeField('Дата регистрации', auto_now_add=True)
    follow = models.ManyToManyField('self', verbose_name='Мои подписки', related_name='following_for', blank=True, symmetrical=False)

    # Управление доступами
    is_staff = models.BooleanField('Имеет доступ к административной панели сайта', default=False)
    # is_active = models.BooleanField('Активен', default=True)
    # active_sig = models.CharField(max_length=50, default='', blank=True)
    is_blocked = models.BooleanField('Заблокирован', default=False,
                                     help_text='Поставьте галочку, чтобы заблокировать пользователя')
    objects = AuthorManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('nickname',)

    # class Meta:
    #     ordering = ['-id', ]
    #     verbose_name = 'профиль'
    #     verbose_name_plural = 'профили'

    def get_full_name(self):
        return self.name or self.nickname

    def get_short_name(self):
        return self.name or self.nickname


    def __str__(self):
        return self.name or self.nickname

    def get_absolute_url(self):
        return reverse('tweets', kwargs={'nickname': self.nickname})

    def photo_url(self):
        return self.photo.url if self.photo else '/static/noimage.png'
