from django.contrib import admin
from .models import Tweet, Commentary, Event

@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
    list_display = ["id", "slug", "image", "tweet_text", "pub_date", "author",]
    list_filter = ["pub_date",]
    search_fields = ["slug",]
    readonly_fields = ["slug",]



@admin.register(Commentary)
class CommentaryAdmin(admin.ModelAdmin):
    list_display = ["tweet", "comment",]


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ["id", "from_who", "to_who", "tweet", "event_type", "datetime"]
    list_filter = ["datetime",]
    search_fields = ["event_type",]
    readonly_fields = ["datetime",]