import datetime
from django.utils import timezone
from django.db import models
from django.core.urlresolvers import reverse
from hashlib import md5



def generate_imagename(self, imgname):
    ext = imgname.split('.')[-1]
    url = "photo.%s" % (ext)
    return url


def generate_photoname(self, imgname):
    ext = imgname.split('.')[-1]
    url = "%s/photo.%s" % (self.nickname, ext)
    return url


class Commentary(models.Model):
    tweet = models.ForeignKey('Tweet')
    pub_date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('registration.Author')
    comment = models.CharField(max_length=170)

    def __str__(self):
        return self.comment


class Tweet(models.Model):
    tweet_text = models.CharField(max_length=170)
    likes = models.ManyToManyField('registration.Author', verbose_name='падабайкi', related_name='favourite_tweets', blank=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=generate_imagename, blank=True)
    slug = models.SlugField(unique=True, help_text="Вычисляется автоматически")
    author = models.ForeignKey('registration.Author')
    reposted_from=models.ForeignKey('self', blank=True, null=True)

    class Meta:
        ordering = ["-pub_date"]

    def save(self, *args, **kwargs):
        if not self.id and not self.slug:
            super(Tweet, self).save(*args, **kwargs)
            current_time = str(datetime.datetime.now().time())
            self.slug = md5(str(str(self.id)+current_time).encode('utf-8')).hexdigest()[:20]
        super(Tweet, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('tweet_by_slug', kwargs={'nickname': self.author.nickname, 'slug': self.slug})

    def get_image_url(self):
        if self.image:
            return self.image.url

    def __str__(self):
        return self.slug


class Author(models.Model):
    name = models.CharField(max_length=50)
    nickname = models.SlugField(unique=True)
    email = models.EmailField(blank=True, unique=True)
    photo = models.ImageField(upload_to=generate_photoname, blank=True)

    def get_absolute_url(self):
        return reverse('tweets', kwargs={'nickname': self.nickname})

    def __str__(self):
        return self.name


class Event(models.Model):
    TYPE_CHOICES = (
        ('like', 'лайк'),
        ('repost', 'репост'),
        ('comment', 'коммент'),
        ('follow', 'подписка'),
        ('unfollow', 'отписка'),
    )
    from_who = models.ForeignKey('registration.Author', verbose_name='Event initiator', related_name='event_initiator', db_index=True)
    to_who = models.ForeignKey('registration.Author', verbose_name='Event target', related_name='event_target',  db_index=True)
    tweet = models.ForeignKey('Tweet', verbose_name='Related tweet', related_name='event_tweet', null=True, blank=True)
    event_type = models.CharField(choices=TYPE_CHOICES, max_length=100)
    datetime = models.DateTimeField(auto_now_add=True)