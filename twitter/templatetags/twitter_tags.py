from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
import re

register = template.Library()


@register.assignment_tag(takes_context=True)
def is_tweet_already_reposted(context, tweet_id):
	request = context['request']
	try:
		return True if tweet_id in request.user.tweet_set.all().values_list('reposted_from',
		                                                                    flat=True) else False
	except Exception:
		return False


@register.filter(needs_autoescape=True)
def tagify(tweet, autoescape=None):
	pattern = "#{1}\\w+\\b"
	tags = re.findall(pattern, tweet)
	if autoescape:
		esc = conditional_escape
	else:
		esc = lambda x: x
	'''wraping with <a>'''
	esc(tweet)
	for tag in tags:
		tweet = tweet.replace(tag, "<a href='/tag/" + tag[1:] + "/'>" + tag + "</a>")
	return mark_safe(tweet)

