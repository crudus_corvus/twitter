"""twitter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from . import views
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^$', views.MainView.as_view(), name='welcome'),
    url(r'^signup/',views.AuthorSignUpView.as_view(), name='signup'),
    url(r'^login/',views.AuthorLoginView.as_view(), name='login'),
    url(r'^logout/',views.AuthorLogoutView.as_view(), name='logout'),
    url(r'^@(?P<nickname>[^/]+)/$', views.AuthorTweetsView.as_view(), name='tweets'),
    url(r'^@(?P<nickname>[^/]+)/(?P<slug>[^/]+)/$', views.TweetView.as_view(), name='tweet_by_slug'),
    url(r'^submitpost/', views.TweetCreateView.as_view(), name='submit_post'),
    url(r'^tag/(?P<tag>[^/]+)/$', views.TagView.as_view(), name='tag_list'),
    url(r'^feed/$', views.FeedView.as_view(), name='feed'),
    url(r'^tweet/(?P<tweet_id>\d+)/like/$', views.AddLikeView.as_view(), name='add_like'),
    url(r'^tweet/(?P<tweet_id>\d+)/unlike/$', views.RemoveLikeView.as_view(), name='remove_like'),
    url(r'^(?P<author_id>\d+)/follow/$', views.FollowView.as_view(), name='follow'),
    url(r'^(?P<author_id>\d+)/unfollow/$', views.UnfollowView.as_view(), name='unfollow'),
    url(r'^tweet/(?P<tweet_id>\d+)/repost/$', views.RepostView.as_view(), name='repost'),
    url(r'^tweet/(?P<tweet_id>\d+)/delete/$', views.DeletePostView.as_view(), name='delete_post'),
    url(r'^tweet/(?P<tweet_id>\d+)/(?P<comment_id>\d+)/delete/$', views.DeleteCommentView.as_view(), name='delete_comment'),
    url(r'^notifications/$', views.NotificationsView.as_view(), name='notifications'),
    url(r'^testpage/$', views.TestPageView.as_view(), name='testpage'),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
