from django.core.context_processors import request
from .models import Tweet
from registration.models import Author


def menu(request):
    return {
        'post': Tweet.objects.all(),
        'thor': Author.objects.all(),
    }