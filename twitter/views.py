from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from .models import Commentary, Tweet, Event
from registration.models import Author
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from .forms import AuthorSignUpForm, TweetCommentsForm, TweetCreateForm, AuthorLoginForm
from django.http import JsonResponse
from django.db import IntegrityError
from django.template.loader import get_template
from django.template import Context
from hashlib import md5


class MainView(generic.TemplateView):
    template_name = "twitter/main.html"


class AuthorSignUpView(generic.CreateView):
    model = Author
    form_class = AuthorSignUpForm
    template_name = "twitter/signup.html"

    def get_success_url(self):
        return reverse('welcome')

    def form_valid(self, form):
        form.save()
        author = authenticate(**{'email': form.cleaned_data['email'], 'password': form.cleaned_data['password']})
        author.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, author)
        return super(AuthorSignUpView, self).form_valid(form)


class AuthorLoginView(generic.FormView):
    form_class = AuthorLoginForm
    template_name = "twitter/login.html"

    def get_success_url(self):
        return reverse('feed')

    def form_valid(self, form):
        author = authenticate(**form.cleaned_data)
        if author:
            login(self.request, author)
        return super(AuthorLoginView, self).form_valid(form)


class AuthorLogoutView(generic.View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(redirect_to=reverse('welcome'))


class AuthorTweetsView(generic.CreateView):
    model = Commentary
    form_class = TweetCommentsForm
    template_name = "twitter/tweets.html"

    def get_context_data(self, **kwargs):
        context = super(AuthorTweetsView, self).get_context_data(**kwargs)
        context['author'] = get_object_or_404(Author, nickname=self.kwargs['nickname'])
        return context

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {'errors': errors, 'result': 'error'}
        return JsonResponse(data)

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'ok'})


class TweetCreateView(generic.CreateView):
    model = Tweet
    form_class = TweetCreateForm
    template_name = "twitter/tweets.html"

    def get_context_data(self, **kwargs):
        context = super(TweetCreateView, self).get_context_data(**kwargs)
        context['author'] = get_object_or_404(Author, nickname=self.kwargs['nickname'])
        tweet_was_reposted = self.request.user.tweet_set.all().values_list('reposted_from', flat=True)
        context['tweet_was_reposted'] = tweet_was_reposted
        return context

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {'errors': errors, 'result': 'error'}
        return JsonResponse(data)

    def form_valid(self, form, *args, **kwargs):
        super(TweetCreateView, self).form_valid(form, *args, **kwargs)
        template = get_template('twitter/js-post_ext.html')
        html = template.render(Context({'tweet':form.instance}))
        return JsonResponse({'result': 'ok', 'html':html})

class TweetView(generic.CreateView):
    model = Tweet
    form_class = TweetCommentsForm
    template_name = "twitter/tweet.html"

    def get_context_data(self, **kwargs):
        context = super(TweetView, self).get_context_data(**kwargs)
        context['tweet'] = get_object_or_404(Tweet, slug=self.kwargs['slug'])
        return context

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {'errors': errors, 'result': 'error'}
        return JsonResponse(data)

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'ok'})


class TagView(generic.ListView):
    model = Tweet
    context_object_name = 'tag_set'
    template_name = "twitter/tags.html"

    def get_queryset(self):
        tag = self.kwargs['tag']
        tag_set = Tweet.objects.filter(tweet_text__icontains="#{0}".format(tag))
        return tag_set.order_by('-pub_date')


class FeedView(LoginRequiredMixin, generic.ListView):
    model = Tweet
    context_object_name = 'tweet_set'
    template_name = "twitter/feed.html"
    login_url = '/login/'
    redirect_field_name = '/main/'

    def get_queryset(self):
        author = self.request.user
        queryset_1 = Tweet.objects.filter(author__in=author.follow.all())
        queryset_2 = author.tweet_set.all()
        tweet_set = queryset_1 | queryset_2
        return tweet_set.order_by('-pub_date')

    def get_context_data(self, **kwargs):
        context = super(FeedView, self).get_context_data(**kwargs)
        context['author'] = get_object_or_404(Author, id=self.request.user.id)
        return context


class AddLikeView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            tweet_id = kwargs['tweet_id']
            tweet = Tweet.objects.get(id=tweet_id)
            author = request.user
            tweet.likes.add(author)
            Event.objects.create(
                from_who = author,
                to_who = tweet.author,
                tweet = tweet,
                event_type = "like",
            )
            return JsonResponse({'result': 'ok', 'count': tweet.likes.count()})
        except (ValueError, KeyError, Tweet.DoesNotExist) as e:
            return JsonResponse({'result': 'error'})


class RemoveLikeView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            tweet_id = kwargs['tweet_id']
            tweet = Tweet.objects.get(id=tweet_id)
            author = request.user
            event = Event.objects.filter(from_who=author, tweet=tweet, event_type="like")
            event.delete()
            tweet.likes.remove(author)
            return JsonResponse({'result': 'ok', 'count': tweet.likes.count()})
        except (ValueError, KeyError, Tweet.DoesNotExist) as e:
            return JsonResponse({'result': 'error'})


class FollowView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            author_id = kwargs['author_id']
            author = Author.objects.get(id=author_id)
            follower = request.user
            follower.follow.add(author)
            Event.objects.create(
                from_who = follower,
                to_who = author,
                event_type = "follow",
            )
            return JsonResponse({'result': 'ok'})
        except (ValueError, KeyError, Tweet.DoesNotExist) as e:
            return JsonResponse({'result': 'error'})


class UnfollowView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            author_id = kwargs['author_id']
            author = Author.objects.get(id=author_id)
            follower = request.user
            follower.follow.remove(author)
            Event.objects.create(
                from_who = follower,
                to_who = author,
                event_type = "unfollow",
            )
            return JsonResponse({'result': 'ok'})
        except (ValueError, KeyError, Tweet.DoesNotExist) as e:
            return JsonResponse({'result': 'error'})


class RepostView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            tweet_id = kwargs['tweet_id']
            tweet = Tweet.objects.get(id=tweet_id)
            if self.request.user != tweet.author:
                Tweet.objects.create(
                    author = self.request.user,
                    reposted_from = tweet,
                    tweet_text = tweet.tweet_text,
                    image = tweet.image if tweet.image else None,
                    slug = str(tweet.slug)+str(self.request.user.id),
                )
                Event.objects.create(
                    from_who = self.request.user,
                    to_who = tweet.author,
                    tweet = tweet,
                    event_type = "repost",
                )
            return JsonResponse({'result': 'ok'})
        except (ValueError, KeyError, Tweet.DoesNotExist, IntegrityError) as e:
            return JsonResponse({'result': e})


class DeletePostView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            tweet_id = kwargs['tweet_id']
            tweet = Tweet.objects.get(id=tweet_id)
            author = tweet.author
            if self.request.user == author:
                tweet.delete()
            return JsonResponse({'result': 'ok'})
        except (ValueError, KeyError, Tweet.DoesNotExist) as e:
            return JsonResponse({'result': e})


class DeleteCommentView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            comment_id = kwargs['comment_id']
            tweet_id = kwargs['tweet_id']
            comment = Commentary.objects.get(id=comment_id)
            author = Tweet.objects.get(id=tweet_id).author
            if self.request.user == author:
                comment.delete()
            return JsonResponse({'result':'ok'})
        except (ValueError, KeyError, Commentary.DoesNotExist) as e:
            return JsonResponse({'result': e})


class NotificationsView(generic.ListView):
    model = Event
    context_object_name = 'event_set'
    template_name = "twitter/notifications.html"

    def get_queryset(self):
        to_who = self.request.user
        event_set = Event.objects.filter(to_who=to_who)
        return event_set.order_by('-datetime')

    def get_context_data(self, **kwargs):
        context = super(NotificationsView, self).get_context_data(**kwargs)
        context['author'] = get_object_or_404(Author, nickname=self.request.user.nickname)
        return context

class TestPageView(generic.TemplateView):
    template_name = "twitter/testpage.html"

    def get_context_data(self, **kwargs):
        context = super(TestPageView, self).get_context_data(**kwargs)
        context['text'] = "a rosa #upala na #lapu azora"
        return context
