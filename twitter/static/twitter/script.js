$(document).ready(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');


    $('.tweet, .reposted_tweet').hover(function(){
        $(this).animate({
            width: "-=1%"
        }, 300);
        $('.js-delete_post').css({'opacity':'0.5'})
    }, function(){
        $(this).animate({
            width: "+=1%"
        }, 300);
        $('.js-delete_post').css({'opacity':'0'})
    });

    $('.commentary').hover(function(){
        $(this).find('.js-delete_comment').css({'opacity':'0.5'})
    }, function(){
        $(this).find('.js-delete_comment').css({'opacity':'0'})
    });

    $('.tweet_text').hover(function() {
        this.style.cursor = 'pointer';
    });

    $('h4').click(function(){
        window.location=$(this).parents().find('.tweet_date').find('a[href]').attr('href');
    });

    /*$('.tweet, .reposted_tweet').click(function(){
        window.location=$(this).find('.tweet_date').find('a[href]').attr('href');
    });*/

    $('.main_block').hide();


    setTimeout(function(){
        $('.uuitor').animate({
            top: "7%",
            left: "82%"
        }, 1000);
        $('.main_block').fadeIn(2000);
    }, 3000);
    clearTimeout();


    $('body').on('submit', '.js-submit_comment', function(event) {
        //форма комментов
            event.preventDefault();
            var d = new Date(),
                $form = $(this),
                data = 'csrfmiddlewaretoken=' + csrftoken + '&' + $form.serialize(),
                url = $form.attr('action'),
                commentary_text = $form.find('input[name="comment"]').val(),
                nickname = $form.attr('data-comment-author-nickname'),
                photo_url = $form.attr('data-comment-author-photo-url'),
                $tweet_wrap = $form.parents('.tweet_wrap'),
                $comment_list = $tweet_wrap.find('div.comment_list'),
                month_to_str=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                formated_date = d.getHours() + ':' + d.getMinutes() + ', ' + d.getDate() + "." + month_to_str[d.getMonth()];

            $.post(url, data, function(res) {
                var err = res['errors'],
                    result = res['result'];
                if (result == 'ok') {
                    $form[0].reset();
                    $comment_list.append("<p style='text-align: left;'> <a href='/@"+ nickname
                        +"'><img style='height: 30px; border-radius:5px; margin: 0px 9px -10px 5px;' src='"
                        + photo_url + "'></a>" + commentary_text +  " " + formated_date + "</p>");
                } else {
                  alert(err);
                }
            });
    });


    $('.js-submit_post').on('submit', function(event) {
        //форма постов
            event.preventDefault();
            var $form = $(this),
                url = $form.attr('action'),
                formData = new FormData($form.get(0)),
                post_wrap = $('.post_form');

            $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                var err = data.error,
                    result = data.result,
                    html = data.html;
                if (result == 'ok') {
                    $form[0].reset();
                    post_wrap.after(html);
                } else {
                    alert(err);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('uwaga! uwaga!');
            }
        });
    });


    $('body').on('click', '.like_button, .bigger_like', function(event) {
        //форма лайков
        event.preventDefault();
        var $button = $(this),
            like_url = $button.attr('data-like-url'),
            dislike_url = $button.attr('data-dislike-url'),
            data = {csrfmiddlewaretoken: csrftoken};

        var url;
        if ($button.hasClass('already_added')) { url = dislike_url }
        else { url = like_url }
        console.log(url);

        $.post(url, data, function(res) {
            var result = res.result;

            if (result == 'ok') {
                $button.toggleClass('already_added');
                $button.find('.like_counter').text(res['count']);
            } else if (result == 'error') {
                alert(res['error']);
            }
        });
    });

    $('body').on('click', '.js-follow_button, .js-unfollow_button', function(event) {
        //форма подписок
        event.preventDefault();
        var $button = $(this),
            follow_url = $button.attr('data-follow-url'),
            unfollow_url = $button.attr('data-unfollow-url'),
            data = {csrfmiddlewaretoken: csrftoken};

        var url;
        if ($button.hasClass('js-follow_button')) { url = follow_url }
        else if ($button.hasClass('js-unfollow_button')) { url = unfollow_url }

        $.post(url, data, function(res) {
            var result = res.result;
            //возможно, на toggleClass было бы лучше?
            if (result == 'ok') {
                if(url==follow_url){
                    $button.removeClass();
                    $button.addClass('js-unfollow_button');
                    $button.val('Unfollow')
                } else {
                    $button.removeClass();
                    $button.addClass('js-follow_button');
                    $button.val('Follow')
                }
            } else if (result == 'error') {
                alert(res['error']);
            }
        });
    });

    $('body').on('click', '.js-repost_button', function(event) {
        //форма репостов
        event.preventDefault();
        var $button = $(this),
            url = $button.attr('data-repost-url'),
            data = {csrfmiddlewaretoken: csrftoken};

        $.post(url, data, function(res) {
            var result = res.result;
            if(result == 'ok') {
                $button.addClass('reposted');
                alert('Reposted!');
            }
        });
    });

    $('body').on('click', '.js-delete_post', function(event) {
        //форма удаления постов
        event.preventDefault();
        var $button = $(this),
            url = $button.attr('data-delete-url'),
            data = {csrfmiddlewaretoken: csrftoken};

        $.post(url, data, function(res) {
            var result = res.result;
            if(result == 'ok') {
                $button.parents('.tweet_wrap').remove()
            } else {
                alert('smthng goes wrong!');
            }
        });
    });

    $('body').on('click', '.js-delete_comment', function(event) {
        //форма удаления комментариев
        event.preventDefault();
        var $button = $(this),
            url = $button.attr('data-delete-comment-url'),
            data = {csrfmiddlewaretoken: csrftoken};

        $.post(url, data, function(res) {
            var result = res.result;
            if(result == 'ok') {
                $button.parents('.commentary').remove()
            } else {
                alert('smthng goes wrong!');
            }
        });
    });



    $('#upload_buffer_button').click(function(){
        $('#upload_image_button').click();
    });


    $('body').find('.already_liked').css({'color':'#00cdff', 'opacity': '0.7'});
    $('body').find('.reposted').css({'color':'#00cdff', 'opacity': '0.9'});


    $('.tweet').find('h6').find('a').each(function(index, tweet){
        if (tweet.text.split('#').length > 1){
            console.log(tweet.text.split('#')[1].split(' ')[0]);
        }
    });
});