from django import forms
from .models import Commentary, Tweet
from registration.models import Author


'''class AuthorSignUpForm(ModelForm):
    class Meta:
        model = Author
        fields = ["name", "nickname", "email", "photo",] '''


class AuthorSignUpForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password',}))
    password_repeat = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password repeat',}))

    class Meta:
        model = Author
        fields = ['name', 'nickname', 'email', 'photo']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'maxlength': 200}),
            'email': forms.TextInput(attrs={'placeholder': 'E-mail', 'maxlength': 100}),
            'nickname': forms.TextInput(attrs={'placeholder': 'Nickname', 'maxlength': 20}),
            'photo': forms.ClearableFileInput(attrs={'placeholder': 'Photo',}),
        }

    def clean_password_repeat(self):
        password = self.cleaned_data.get('password')
        password_repeat = self.cleaned_data.get('password_repeat')

        if password and password_repeat and password != password_repeat:
            raise forms.ValidationError('Пароли не совпадают')

        return password_repeat

    def save(self, commit=True):
        author = super(AuthorSignUpForm, self).save(commit=False)
        author.set_password(self.cleaned_data['password'])

        if commit:
            author.save()

        return author


class AuthorLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder':'E-mail',}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password',}))

    def clean(self):
        cleaned_data = super(AuthorLoginForm, self).clean()
        if not self._errors:
            email = cleaned_data.get('email')
            password = cleaned_data.get('password')
            try:
                author = Author.objects.get(email=email)
                if not author.check_password(password):
                    raise forms.ValidationError('wrong')
                elif author.is_blocked:
                    raise forms.ValidationError('blocked')
            except Author.DoesNotExist:
                raise forms.ValidationError('wrong')
        return cleaned_data


class TweetCommentsForm(forms.ModelForm):
    class Meta:
        model = Commentary
        fields = ["comment", "author", "tweet",]


class TweetCreateForm(forms.ModelForm):
    class Meta:
        model = Tweet
        fields = ["tweet_text", "author", "image",]


class TweetRepostForm(forms.ModelForm):
    class Meta:
        model = Tweet
        fields = ["reposted_from"]
