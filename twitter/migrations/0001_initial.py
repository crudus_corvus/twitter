# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import twitter.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.SlugField(unique=True)),
                ('email', models.EmailField(blank=True, max_length=254, unique=True)),
                ('photo', models.ImageField(upload_to=twitter.models.generate_photoname, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Commentary',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('pub_date', models.DateTimeField(auto_now_add=True)),
                ('comment', models.CharField(max_length=170)),
                ('author', models.ForeignKey(to='twitter.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Tweet',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('tweet_text', models.CharField(max_length=170, unique=True)),
                ('likes', models.IntegerField(blank=True, null=True)),
                ('pub_date', models.DateTimeField(auto_now_add=True)),
                ('image', models.ImageField(upload_to=twitter.models.generate_imagename, blank=True)),
                ('slug', models.SlugField()),
            ],
            options={
                'ordering': ['-pub_date'],
            },
        ),
        migrations.AddField(
            model_name='commentary',
            name='tweet',
            field=models.ForeignKey(to='twitter.Tweet'),
        ),
        migrations.AddField(
            model_name='author',
            name='tweet',
            field=models.ManyToManyField(to='twitter.Tweet'),
        ),
    ]
