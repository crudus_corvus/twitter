# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0014_auto_20160217_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='slug',
            field=models.SlugField(unique=True, help_text='Вычисляется автоматически'),
        ),
    ]
