# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0018_events'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('event_type', models.CharField(choices=[('like', 'лайк'), ('repost', 'репост'), ('comment', 'коммент')], max_length=100)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('from_who', models.ForeignKey(related_name='event_initiator', verbose_name='Event initiator', to='twitter.Author')),
                ('to_who', models.ForeignKey(related_name='event_target', verbose_name='Event target', to='twitter.Author')),
                ('tweet', models.ForeignKey(related_name='event_tweet', verbose_name='Related tweet', to='twitter.Tweet')),
            ],
        ),
        migrations.RemoveField(
            model_name='events',
            name='from_who',
        ),
        migrations.RemoveField(
            model_name='events',
            name='to_who',
        ),
        migrations.RemoveField(
            model_name='events',
            name='tweet',
        ),
        migrations.DeleteModel(
            name='Events',
        ),
    ]
