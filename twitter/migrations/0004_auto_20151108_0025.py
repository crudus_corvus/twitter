# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0003_auto_20151108_0011'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tweet',
            old_name='poster',
            new_name='author',
        ),
    ]
