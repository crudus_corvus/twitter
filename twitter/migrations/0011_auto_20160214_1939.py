# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0010_auto_20160214_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='reposted_from',
            field=models.ForeignKey(to='twitter.Tweet', blank=True, null=True),
        ),
    ]
