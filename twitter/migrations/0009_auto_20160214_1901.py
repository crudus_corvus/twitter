# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0008_tweet_reposted_from'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='tweet_text',
            field=models.CharField(max_length=170),
        ),
    ]
