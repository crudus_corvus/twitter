# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0004_auto_20151108_0025'),
    ]

    operations = [
        migrations.RenameField(
            model_name='author',
            old_name='slug',
            new_name='nickname',
        ),
        migrations.AlterField(
            model_name='tweet',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
