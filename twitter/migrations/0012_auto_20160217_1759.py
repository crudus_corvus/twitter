# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0011_auto_20160214_1939'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='slug',
            field=models.SlugField(),
        ),
        migrations.AlterUniqueTogether(
            name='tweet',
            unique_together=set([('slug', 'pub_date')]),
        ),
    ]
