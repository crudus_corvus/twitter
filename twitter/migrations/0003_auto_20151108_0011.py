# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0002_auto_20151107_1909'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tweet',
            old_name='author',
            new_name='poster',
        ),
    ]
