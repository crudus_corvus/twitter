# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0019_auto_20160304_0224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='event_type',
            field=models.CharField(max_length=100, choices=[('like', 'лайк'), ('repost', 'репост'), ('comment', 'коммент'), ('follow', 'подписка'), ('unfollow', 'отписка')]),
        ),
        migrations.AlterField(
            model_name='event',
            name='from_who',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='event_initiator', verbose_name='Event initiator'),
        ),
        migrations.AlterField(
            model_name='event',
            name='to_who',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='event_target', verbose_name='Event target'),
        ),
        migrations.AlterField(
            model_name='event',
            name='tweet',
            field=models.ForeignKey(blank=True, to='twitter.Tweet', related_name='event_tweet', null=True, verbose_name='Related tweet'),
        ),
    ]
