# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0007_auto_20160204_1951'),
    ]

    operations = [
        migrations.AddField(
            model_name='tweet',
            name='reposted_from',
            field=models.ForeignKey(to='twitter.Tweet', null=True),
        ),
    ]
