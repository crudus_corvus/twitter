# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0013_auto_20160217_1817'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='tweet',
            unique_together=set([]),
        ),
    ]
