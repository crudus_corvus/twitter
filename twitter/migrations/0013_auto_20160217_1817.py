# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0012_auto_20160217_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='slug',
            field=models.SlugField(editable=False, help_text='Вычисляется автоматически', unique=True),
        ),
    ]
