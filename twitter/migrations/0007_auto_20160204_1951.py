# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import twitter.models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0006_auto_20160203_0229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='image',
            field=models.ImageField(blank=True, default='/static/noimage.png', upload_to=twitter.models.generate_imagename),
        ),
    ]
