# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twitter', '0017_auto_20160301_0319'),
    ]

    operations = [
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('event_type', models.CharField(choices=[('like', 'лайк'), ('repost', 'репост'), ('comment', 'коммент')], max_length=100)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('from_who', models.ForeignKey(verbose_name='Event initiator', related_name='event_initiator', to='twitter.Author')),
                ('to_who', models.ForeignKey(verbose_name='Event target', related_name='event_target', to='twitter.Author')),
                ('tweet', models.ForeignKey(verbose_name='Related tweet', related_name='event_tweet', to='twitter.Tweet')),
            ],
        ),
    ]
